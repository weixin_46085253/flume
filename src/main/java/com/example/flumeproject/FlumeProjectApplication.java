package com.example.flumeproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlumeProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlumeProjectApplication.class, args);
    }

}
